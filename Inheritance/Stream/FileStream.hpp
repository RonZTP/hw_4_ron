#pragma once
#include "OutStream.hpp"

#define DEFAULT_FILE_NAME_SIZE 1000

class FileStream : public OutStream
{
public:
	FileStream();
	~FileStream();
    
    FileStream& operator <<(const char *text);
private:
    FILE* _writeToFile;
    char _fileName[DEFAULT_FILE_NAME_SIZE];
};

