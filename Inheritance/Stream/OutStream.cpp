#include "OutStream.hpp"


OutStream::OutStream()
{
	_file = stdout;
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(_file, "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(_file, "%d", num);
	return *this;
}

OutStream& OutStream::operator << (void(*pf)(FILE* file))
{
    pf(this->_file);
    return *this;
}

void endline(FILE* myFile)
{
	fprintf(myFile , "%c", '\n');
}

