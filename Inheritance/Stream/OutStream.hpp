#pragma once
#include <stdio.h>
#include <string.h>
class OutStream
{
public:
	OutStream();
	~OutStream();
	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE* file));
protected:
	FILE* _file;
};

void endline(FILE* myFile);
