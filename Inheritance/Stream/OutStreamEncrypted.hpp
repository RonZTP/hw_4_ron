
#pragma once
#include "OutStream.hpp"

#define SMALLEST_VALUE 32
#define LARGEST_VALUE 126

class OutStreamEncrypted: public OutStream
{
public:
    OutStreamEncrypted();
    ~OutStreamEncrypted();
    //OutStream& operator<<(const char *str);
    void operator<<(char *str)
    {
        size_t len = strlen(str);
        
		char* string1 = new char[len];
		
		strcpy(string1, str); //Function not found
	   
        int currentLetterVal = 0;
        char currentLetterChar = 'a';
        
        for(int i = 0; i < len; i++)
        {
            currentLetterVal = (int)str[i]; 
            if(!(currentLetterVal < 32 || currentLetterVal > LARGEST_VALUE))
            {
                currentLetterVal = (currentLetterVal + _offset) % LARGEST_VALUE;
				currentLetterChar = (char)currentLetterVal;
				string1[i] = currentLetterChar;
            }
        }
        OutStream::operator<<(string1);
		delete[] string1;
    }
    private:
    int _offset;
};
