#include "FileStream.hpp"

FileStream::FileStream()
{
    fprintf(stdout, "Enter file name: ");
    fscanf(stdin, "%s", _fileName);
}

FileStream::~FileStream()
{
}

FileStream& FileStream::operator <<(const char *text)
{
    _writeToFile = fopen(_fileName, "w");
    fprintf(_writeToFile, "%s", text);
    fclose(_writeToFile);
    return *this;
}
