#include "OutStream.hpp"
#include "FileStream.hpp"
#include "OutStreamEncrypted.hpp"

#define SCREEN stdout

int main(int argc, char **argv)
{
	OutStream *out_s = new OutStream(); //Calling the c'tor.
    // FileStream *in_s = new FileStream(); TODO:remove comment
    OutStreamEncrypted *out_s_enc = new OutStreamEncrypted();
    
    *out_s << endline;
	*out_s << "I am the Doctor and I'm " << 1500 << " years old";
    *out_s << endline;
    
    // *in_s << "\nI am the Doctor and I'm 1500 years old"; TODO:remove comment
    
    *out_s_enc << (char*)"\nI am the Doctor and I'm 1500 years old";
    
	delete out_s_enc;
	delete out_s; // Deleting the object.
	return 0;
}
